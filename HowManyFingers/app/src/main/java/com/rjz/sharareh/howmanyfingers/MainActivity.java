package com.rjz.sharareh.howmanyfingers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn_go;
    static int counter;
    RadioGroup radioGroup_level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //------------------------------------------------------------------------------------

        radioGroup_level = (RadioGroup) findViewById(R.id.main_radioGroup_level);
        radioGroup_level.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.main_easy:
                        counter=10;
                        break;
                    case R.id.main_medium:
                        counter=7;
                        break;
                    case R.id.main_hard:
                        counter=5;
                        break;
                }
            }
        });
        //-------------------------------------------------------------------------------------
        btn_go = (Button) findViewById(R.id.main_go);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gopage = new Intent(MainActivity.this, Game.class);
                startActivity(gopage);
            }
        });


    }
}
