package com.rjz.sharareh.howmanyfingers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Game extends AppCompatActivity {
    EditText editText_num;
    TextView tvshowmsg, tvcountnum,tvresult;
    Button btn_guess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        tvcountnum = (TextView) findViewById(R.id.game_counter);
        tvshowmsg = (TextView) findViewById(R.id.game_msg);
        tvresult = (TextView) findViewById(R.id.game_tvresult);
        editText_num = (EditText) findViewById(R.id.game_guess_num);
        btn_guess = (Button) findViewById(R.id.game_guess);
        //====================================================================================
        tvcountnum.setText(MainActivity.counter+"");
        //====================================================================================
        btn_guess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                --MainActivity.counter;
                tvcountnum.setText(MainActivity.counter+"");
                if (MainActivity.counter == 0) {
                    tvresult.setText("Time Out");
                    btn_guess.setText("Reset");
                    Intent reset = new Intent(Game.this, MainActivity.class);
                    startActivity(reset);
                }else {

                int input = Integer.parseInt(editText_num.getText().toString());
                final Random random = new Random();
                final int r = random.nextInt(10);
                if (input < r) {
                    tvshowmsg.setText("bigger");
                } else if (input > r) {
                    tvshowmsg.setText("smaller");
                } else if (input == r) {
                    tvshowmsg.setText("youwon");
                }
            }}
        });

    }
}
